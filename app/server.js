const { ApolloServer } = require('apollo-server');
const firebase = require('firebase-admin');

const database = require('./config/database');
const firebaseAccount = require('./config/firebase.json');

const logger = require('./helpers/logger');
const config = require('./config');

(async function () {
    // Fire base
    firebase.initializeApp({
        credential: firebase.credential.cert(firebaseAccount),
        databaseURL: 'https://hotdatafb-default-rtdb.asia-southeast1.firebasedatabase.app'
    });

    // Load database
    await database.connectDB();
    await database.loadModelFiles();

    // GraphQL
    const resolvers = require('./resolvers');
    const typeDefs = require('./schemas');

    const server = new ApolloServer({ typeDefs, resolvers });
    server.listen(config.port).then(({ url }) => {
        logger.info(`Server ready at ${url}`);
    });
})();
