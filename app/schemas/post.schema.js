const { gql } = require("apollo-server");

const postSchema = gql`
    type Post {
        _id: String
        postId: String
        categories: [String]
    }

    input CreatePostInput {
        postId: String!
        categories: [String]
        note: String
        typeScan: String!
        startTime: Date
        scanInterval: Int
        accessToken: [String]!
    }

`;

const postAction = gql`
    extend type Query {
        post: [Post]
    }

    extend type Mutation {
        createPost(inputs: CreatePostInput!): Post
    }
`;

module.exports = [postSchema, postAction];
