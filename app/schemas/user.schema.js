const { gql } = require('apollo-server');

const userSchema = gql`
    type MetaDataUser {
        creationTime: String,
        lastSignInTime: String
    }
    type User {
        uid: String,
        email: String,
        emailVerified: Boolean,
        metadata: MetaDataUser
    }
    type sessionLoginResponse{
        sessionCookie: String!
    }
`;

const userAction = gql`
    extend type Query {
        users: [User],
        getUsers: [User]
    }
    extend type Mutation {
        getUsers(nextPageToken: String): [User],
        sessionLogin(token: String!): sessionLoginResponse
    }

`;

module.exports = [userSchema, userAction];
