const _ = require('lodash')
const path = require("path");
const { gql } = require("apollo-server");

const INITIAL_SCHEMA = gql`
    type Query {
        test: Boolean
    }
    type Mutation {
        test: Boolean
    }

    scalar Date

`

const getGlobbedFiles = require('./../utils/getGlobbedFiles')

const schemaPaths = getGlobbedFiles('./schemas/*.schema.js');
const typeDefs = _.reduce(schemaPaths, (result, schemaPath) => {
    const currentSchemas = require(path.resolve(schemaPath));
    result = [...result, ...currentSchemas];
    return result;
}, [INITIAL_SCHEMA])

module.exports = typeDefs
