const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const accountFBSchema = new Schema({
    fbId: String,
    name: String,
    username: String,
    password: String,
    twoFa: String,
    cookie: String,
    fbDtsg: String,
    accessToken: String,
    createdBy: String
}, {
    timestamps: true
});

mongoose.model('AccountFB', accountFBSchema);
