const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { TYPE_SCAN_ALL_COMMENT, TYPE_SCAN_HIDE_COMMENT } = require('../constants');

const postSchema = new Schema({
    postId: String,
    categories: [
        {
            type: mongoose.ObjectId
        }],
    note: String,
    typeScan: {
        type: String,
        enums: [TYPE_SCAN_HIDE_COMMENT, TYPE_SCAN_ALL_COMMENT]
    },
    startTime: Date,
    scanInterval: Number,
    accessTokens: [mongoose.ObjectId],
    comments: [
        {
            commentId: String,
            content: String,
            from: {
                uid: String,
                user: String,
                gender: String
            },
            phones: [String],
            note: String,
            tags: [mongoose.ObjectId],
            scanAt: Date,
            createdAt: Date
        }
    ],
    createdBy: String
}, {
    timestamps: true
});

mongoose.model('Post', postSchema);

