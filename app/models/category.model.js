const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const categorySchema = new Schema({
    name: String,
    note: String,
    color: String,
    createdBy: String
}, {
    timestamps: true
});

mongoose.model('Category', categorySchema);



