const path = require('path');
const mongoose = require('mongoose');

const config = require('../config');
const logger = require('../helpers/logger');

const getGlobbedFiles = require('../utils/getGlobbedFiles');

exports.connectDB = () => {
    return mongoose.connect(config.mongodbURL, { useNewUrlParser: true, useUnifiedTopology: true }).then(() => {
        logger.info('Connected MongoDB');
    }).catch(() => {
        logger.error('Can\'t connect to MongoDB server');
    });
};

exports.loadModelFiles = function () {
    getGlobbedFiles('./models/**/*.js').forEach(function (modelPath) {
        require(path.resolve(modelPath));
    });
};


