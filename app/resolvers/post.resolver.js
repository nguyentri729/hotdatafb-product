const mongoose = require('mongoose');
const Post = mongoose.model('Post');

const createPost = async (_, { inputs }) => {
    const post = await new Post(inputs).save();
    return post;
};

const postResolvers = {
    Mutation: {
        createPost
    }
};

module.exports = postResolvers;
