const { GraphQLScalarType, Kind } = require('graphql');

const dateScalar = new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    serialize(value) {
        return value.getTime();
    },
    parseValue(value) {
        return new Date(value);
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            case Kind.STRING:
                return new Date(ast.value);
            case Kind.INT:
                return new Date(parseInt(ast.value, 10));
            default:
                throw new Error('Please enter a valid date format');
        }
    }
});

const scalarResolver = {
    Date: dateScalar
};

module.exports = scalarResolver;
