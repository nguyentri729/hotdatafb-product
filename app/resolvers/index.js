const _ = require('lodash')
const path = require("path");
const getGlobbedFiles = require('./../utils/getGlobbedFiles')

const schemaPaths = getGlobbedFiles('./resolvers/*.resolver.js');
const resolvers = _.reduce(schemaPaths, (result, resolverPath) => {
    const currentResolver = require(path.resolve(resolverPath));
    result = [...result, currentResolver];
    return result;
}, [])

module.exports = resolvers

