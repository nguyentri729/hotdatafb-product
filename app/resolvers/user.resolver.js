const firebase = require("firebase-admin");

const getUsers = async (_, {nextPageToken}) => {
    const {users} = await firebase.auth().listUsers(1000, nextPageToken)
    return users
}

const sessionLogin = async (_, {token}) => {
    const decodedIdToken = await firebase.auth().verifyIdToken(token);
    if ( new Date().getTime() / 1000 - decodedIdToken.auth_time < 5 * 60) {
        const expiresIn = 60 * 60 * 24 * 5 * 1000;
        const sessionCookie = await firebase.auth().createSessionCookie(token, {expiresIn })
        return {
           sessionCookie
        }
    }
};

const userResolvers = {
    Query: {
        users: () => {
            return [
                {
                    name: 'tri',
                    age: 10
                }];
        },
        getUsers: () => {
            return [
                {
                    email: 'nguyentriuasd@jhc.om',
                    uid: '213132'
                }];
        }
    },
    Mutation: {
        getUsers,
        sessionLogin
    }
};

module.exports = userResolvers;
