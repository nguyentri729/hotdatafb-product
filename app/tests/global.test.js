const { ApolloServer } = require('apollo-server');

const database = require('../config/database');
const logger = require('../helpers/logger');
const config = require('../config');

exports.mochaHooks = {
    beforeEach(done) {
        done();
    }
};

exports.mochaGlobalSetup = async function () {
    // Load database
    await database.connectDB();
    await database.loadModelFiles();

    const resolvers = require('../resolvers');
    const typeDefs = require('../schemas');

    global.graphQLServer = new ApolloServer({ typeDefs, resolvers });
};
