const dotenv = require('dotenv');
dotenv.config();

const environments = {
    all: {
        mongodbURL: process.env.MONGODB_URL,
        nodeEnv: process.env.NODE_ENV,
        logger: process.env.LOGGER || true,
        port: process.env.PORT || 4000
    },
    development: {
        mongodbURL: process.env.MONGODB_URL,
        port: process.env.PORT || 4000
    },
    test: {
        mongodbURL: process.env.MONGODB_URL_TEST
    }
};

module.exports = Object.assign(
    environments.all,
    environments[process.env.NODE_ENV]
);

