import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "@components/App";
import store from "./store";
import ThemeContextProvider from "./context";
import "./ant.less";
import classes from "./index.css";
import { Button } from "antd";

console.log({ classes });
ReactDOM.render(
  <React.Fragment>
    <Provider store={store}>
      <ThemeContextProvider>
        <App />
        <Button type="primary">Primary Button</Button>
        <h1 className={classes.heading}>say hello world</h1>
      </ThemeContextProvider>
    </Provider>
  </React.Fragment>,
  document.getElementById("root")
);
