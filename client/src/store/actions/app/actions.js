import { createActions } from "redux-actions";

const prefix = "APP";
const actionTypes = ["SET_TITLE"];

const actionMap = {
  SET_SIDEBAR: {
    REQUEST: null,
    RECEIVE: null,
    REJECT: null
  }
};

export default createActions(actionMap, ...actionTypes, { prefix });
